import requests
from dataclasses import dataclass
from datetime import datetime

API_URL = 'http://example.org/'
LOG_FILE_NAME = 'product_log.txt'


@dataclass
class Product:
    id: int
    name: str
    description: str = ''

    def serialize(self) -> dict:
        return {'id': self.id, 'name': self.name}


def dump_product_event(product: Product, success: bool) -> None:
    result = 'SUCCESS' if success else 'FAILURE'
    with open(LOG_FILE_NAME, 'a') as f:
        f.write(f'{datetime.now} {product.id} {product.name} {result}')


def sync_products(products: list[Product]) -> None:
    for product in products:
        response = requests.post(API_URL, data=product.serialize())
        if response.status_code == 200:
            continue
        elif response.status_code == 201:
            dump_product_event(product, success=True)
        else:
            dump_product_event(product, success=False)
