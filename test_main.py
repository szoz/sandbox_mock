from unittest.mock import patch, call

from main import sync_products, Product


@patch('main.dump_product_event')
@patch('main.requests.post')
def test_sync_product_success(mock_post, mock_dump):
    mock_post.return_value.status_code = 201
    products = [Product(id=1, name='Chair'),
                Product(id=2, name='Desk', description='new')]

    sync_products(products)

    assert mock_dump.call_count == 2
    assert mock_dump.call_args_list[0] == call(products[0], success=True)
    assert mock_dump.call_args_list[1] == call(products[1], success=True)
